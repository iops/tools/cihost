# Dockerfile for building install packages


# target os image (used for downloading packages only)
ARG SYS_IMAGE="centos:7"

# ==============================================================================
FROM ${SYS_IMAGE} AS pkgImage
LABEL Description="Image used to download sys packages target OS"
# ==============================================================================

# user-defined build arguments
# ============================
# -- system name
ARG SYS_NAME="RHEL7"

# -- build workspace
ARG BUILD_PATH="/workspace"

# -- final output path
ARG OUT_PATH="/cihost-install"

# -- temporary path
ARG TMP_PATH="/tmp"


# build arguments & preparation tasks
# ===================================
# -- copy required files to workspace
COPY sys ${BUILD_PATH}/sys/
RUN  ls -lah ${BUILD_PATH}/*
# -- run pre-build tasks
RUN if [ "${SYS_NAME}" = "ALP3X" ];then apk add bash;fi


# download os packages
# ====================
ARG PKG_SKIP="false"
ARG PKG_PATH="${BUILD_PATH}/pkg"
ENV PKG_PATH ${PKG_PATH}
WORKDIR ${PKG_PATH}
RUN [[ "true" = "${PKG_SKIP}" ]] || bash ${BUILD_PATH}/sys/${SYS_NAME}/download.packages.sh; ls -lah



# target os image (used for building)
ARG SYS_IMAGE="centos:7"

# ==============================================================================
FROM ${SYS_IMAGE} AS sysImage
LABEL Description="Image used to collect requirements & files for target OS"
# ==============================================================================

# user-defined build arguments
# ============================
# -- system name
ARG SYS_NAME="RHEL7"

# -- build workspace
ARG BUILD_PATH="/workspace"

# -- final output path
ARG OUT_PATH="/cihost-install"

# -- temporary path
ARG TMP_PATH="/tmp"


# build arguments & preparation tasks
# ===================================
# -- common parameters for requirements.sh script
ARG REQUIREMENTS_PARAMETERS="--tmp-path ${TMP_PATH}"

# -- copy required files to workspace
COPY requirements.* tools.* ${BUILD_PATH}/
COPY sys ${BUILD_PATH}/sys/
COPY src ${BUILD_PATH}/src/
RUN  ls -lah ${BUILD_PATH}/*

# -- run pre-build tasks
RUN if [ "${SYS_NAME}" = "ALP3X" ];then apk add bash;fi


# main build tasks
# ================
# -- download git repos to build dir
ARG GIT_SKIP="false"
RUN [[ "true" = "${GIT_SKIP}" ]] || bash ${BUILD_PATH}/requirements.sh ${REQUIREMENTS_PARAMETERS} --dst-path ${BUILD_PATH}/git git

# -- download & package docker images
ARG IMG_SKIP="false"
RUN [[ "true" = "${IMG_SKIP}" ]] || bash ${BUILD_PATH}/requirements.sh ${REQUIREMENTS_PARAMETERS} --dst-path ${BUILD_PATH}/img img

# -- create virtualenv
ARG ENV_SKIP="false"
ARG ENV_PATH="${BUILD_PATH}/env"
RUN if [[ "true" != "${ENV_SKIP}" ]];then \
 # create virtualenv
 pip install virtualenv \
 && virtualenv --activators bash --prompt "cihost" ${ENV_PATH} \
 && if [[ ! -e ${ENV_PATH}/bin/pip ]] && [[ -e ${ENV_PATH}/bin/pip3 ]];then ln -s ${ENV_PATH}/bin/pip3 ${ENV_PATH}/bin/pip && echo "[>] pip symlink created (${ENV_PATH}/bin/pip => ${ENV_PATH}/bin/pip3)";fi \
 # install required pip modules into virtualenv
 && source ${ENV_PATH}/bin/activate \
 && ${ENV_PATH}/bin/pip list \
 && ${ENV_PATH}/bin/pip install --upgrade pip \
 && ${ENV_PATH}/bin/pip install --upgrade -r ${BUILD_PATH}/requirements.pip \
 # enable access to system packages
 && sed -i "s#include-system-site-packages =.*#include-system-site-packages = true#" ${ENV_PATH}/pyvenv.cfg \
 # copy other config files into virtualenv
 && mkdir -p ${ENV_PATH}/cfg \
 && cp -v ${BUILD_PATH}/src/files/ansible.* ${ENV_PATH}/cfg/ \
 && ${ENV_PATH}/bin/pip freeze > ${ENV_PATH}/cfg/requirements.txt \
 && mv ${BUILD_PATH}/src/files/virtualenv.* ${ENV_PATH}/ \
 # display virtualenv status
 && ${ENV_PATH}/bin/pip list \
 && ansible --version \
 && deactivate \
 && ls -lah ${ENV_PATH}/bin \
 ;fi

# -- add ext tools to virtualenv
WORKDIR ${ENV_PATH}/bin
ARG EXT_SKIP="false"
RUN [[ "true" = "${EXT_SKIP}" ]] || bash ${BUILD_PATH}/requirements.sh ${REQUIREMENTS_PARAMETERS} --dst-path ${ENV_PATH} ext \
 && ls -lah


# copy files to final out dir
# ===========================
# -- local source files
WORKDIR ${OUT_PATH}
COPY src/files/ .

# -- generated files
RUN cp -v ${BUILD_PATH}/ansible.* ${OUT_PATH}

# -- git repositories
WORKDIR ${OUT_PATH}/git
RUN for DIRNAME in $(find ${BUILD_PATH}/git -maxdepth 1 -type d -printf '%P\n'); \
  do \
    tar -C ${BUILD_PATH}/git -cvzf ${DIRNAME}.tgz ${DIRNAME}; \
  done; \
  ls -lah

# -- docker images
WORKDIR ${OUT_PATH}/img
RUN for IMAGE in ${BUILD_PATH}/img/*.tgz; \
  do \
    cp -v ${IMAGE} ./; \
  done; \
  ls -lah

# -- downloaded packages
COPY --from=pkgImage ${BUILD_PATH}/pkg ${OUT_PATH}/pkg

# -- virtualenv & tools
RUN cp -r ${BUILD_PATH}/env ${OUT_PATH}/

# -- used requirements files
WORKDIR ${OUT_PATH}/src
RUN cp -rv ${BUILD_PATH}/requirements.*  ./
RUN cp -rv ${BUILD_PATH}/sys/${SYS_NAME} ./

# -- cleanup
RUN rm -f ${OUT_PATH}/virtualenv.*


# create final release bundle
# ===========================
ARG BUNDLE_NAME="release.tgz"
ARG BUNDLE_PATH="/"
ENV BUNDLE_FILE="${BUNDLE_PATH}/${BUNDLE_NAME}"
# creating archive
RUN ls -lah ${OUT_PATH}; \
  if [[ -f "${BUNDLE_FILE}" ]];then \
    echo "[i] release bundle file [${BUNDLE_FILE}] already exists, skipping..."; \
  else \
    echo "[i] creating release bundle file [${BUNDLE_FILE}]..." \
    && tar -C $( dirname ${OUT_PATH}) -cvzf ${BUNDLE_FILE} $( basename ${OUT_PATH}) \
    && ls -lah ${BUNDLE_FILE}; \
  fi

# show build workspace files
RUN ls -lah ${BUILD_PATH}/

# switch to output dir
WORKDIR ${OUT_PATH}

# show build output files
RUN ls -lah ${OUT_PATH}/


# eof
