# Dockerfile for building requirements baseimage
# USAGE: docker build -t requirements:rhel7 -f RHEL7.Dockerfile .


# target os image (used for building)
ARG SYS_IMAGE="centos:7"

# ==============================================================================
FROM ${SYS_IMAGE}
LABEL Description="requirements baseimage"
# ==============================================================================

# user-defined build arguments
# ============================
# -- system name
ARG SYS_NAME="RHEL7"

# -- build workspace
ARG BUILD_PATH="/workspace"

# -- final output path
ARG OUT_PATH="/out"

# -- temporary path
ARG TMP_PATH="/tmp"

# -- copy only requirement script to workspace
COPY requirements.sh ${BUILD_PATH}/

# -- run requirement script to install self dependencies
RUN bash ${BUILD_PATH}/requirements.sh --dst-path ${BUILD_PATH} --tmp-path ${TMP_PATH} -v

# switch to build path
WORKDIR ${BUILD_PATH}

# eof
