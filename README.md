# cihost

**cihost** is a project for setting up an all-in-one CI/CD **control-host** for docker-based use-cases

# usage

## 1) create installation package

### prerequisites
For building the `offline installation package`, you will need a linux/osx host with the following software installed:
- bash 4 or higher
- docker 17.05 or higher (server + client)
- git (optional, for cloning the repository)

### clone/download the git repository
```bash
git clone git@<gitserver>:path/to/cihost.git
```

### build release for your target os (ex: EL7 systems)
```bash
./build.sh --help
./build.sh RHEL7
```
the final installation package will be saved under **<CURRENT_DIR>/out/release-<SYS_NAME>-<VERSION>.tgz** (you can override the output dir path using the switch `--out-path PATH`)

## 2) start installation on target system

### prerequisites
Ther target system that will be used as `cihost` should meet the following requirements:
- at least 2x  vCPU
- at least 4GB vRAM
- at least 20GB free disk space
  - 50GB or more are  recommended for storing packages & registry
  - you can use an own mountpoint under `/opt/services`
- bash 4 or higher installed to store services's data

### copy the release archive to a temp location on the target host
```bash
scp out/release-<SYS_NAME>-<VERSION>.tgz root@<cihost>:/tmp/
```

### connect to the target host
```bash
ssh root@<cihost>
```

### unpack the installation package
```bash
tar xvzf /tmp/release-<SYS_NAME>-<VERSION>.tgz
```

### start the installation
```bash
/tmp/cihost-install/install.sh --help
/tmp/cihost-install/install.sh
```
