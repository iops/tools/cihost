#!/usr/bin/env bash
PURPOSE="download required OS packages"

# -- auto: path variables
scriptSelf=$0;
scriptName=$(basename $scriptSelf)
scriptBaseName=$(basename $scriptName)
scriptCallDir=$(dirname $scriptSelf)
scriptFullDir=$(cd $scriptCallDir;echo $PWD)
scriptFullPath=$scriptFullDir/$scriptName;
scriptParentDir=$(dirname $scriptFullDir)
# -- /auto: path variables


# +------------------+
# | check OS release |
# +------------------+

EL_RELEASE=$(cat /etc/redhat-release 2>&1)
EL_RELEASE_NAME=${EL_RELEASE%% release *}
EL_RELEASE_VERSION=${EL_RELEASE##* release }
EL_RELEASE_VERSION_MAJOR=$(awk -F'.' '{print $1}' <<<$EL_RELEASE_VERSION)
EL_RELEASE_VERSION_MINOR=$(awk -F'.' '{print $2}' <<<$EL_RELEASE_VERSION)
echo "[>] RUNNING on OS: $EL_RELEASE_NAME $EL_RELEASE_VERSION"


# +-----------------+
# | setup work dirs |
# +-----------------+

[[ -n "$TMP_PATH" ]] || TMP_PATH="/tmp"
# where to put packages
[[ -n "$PKG_PATH" ]] || PKG_PATH="${scriptFullDir}/pkg"
[[ -d "$PKG_PATH" ]] || mkdir -p ${PKG_PATH}


# exit on errors
set -e
report_errors() { echo -e "\033[31m] ERROR occured on line [\033[33m${1}\033[31m]\033[0m"; }
trap 'report_errors ${LINENO}' ERR


# +--------------------+
# | download updates   |
# +--------------------+

# -- download all packages that should be updated first
echo "[>] downloaded updated packages using yum update"
yum update --nogpgcheck --downloadonly --downloaddir=${PKG_PATH}/ -y


# +--------------------+
# | preparation tasks  |
# +--------------------+

# -- install packages required for build run
BUILD_PACKAGES="createrepo curl epel-release yum-utils"
echo "[>] installing BUILD_PACKAGES"
yum update -y
yum repolist
sleep 3
yum install -y ${BUILD_PACKAGES}

# -- setup BUILD_REPOSITORIES
for REPO in $(grep -vE "^(#.*|$)" ${scriptFullDir}/repositories.url)
do
  echo "[>] installing repo: ${REPO%%;*}"
  curl -kLo "/etc/yum.repos.d/${REPO%%;*}.repo" "${REPO##*;}"
  yum makecache -y fast
done


# +----------------------+
# | download OS packages |
# +----------------------+

OS_PACKAGES=$(grep -vE "^(#.*|$)" ${scriptFullDir}/requirements.pkg)

# -- download each required packages with their dependencies first
echo "[>] initially download all required packages using yum install (see output.yum-1st-download.log)"
yum install   --nogpgcheck --downloadonly --downloaddir=${PKG_PATH}/ -y ${OS_PACKAGES}
yum reinstall --nogpgcheck --downloadonly --downloaddir=${PKG_PATH}/ -y ${OS_PACKAGES} || true

# -- reinstall each already downloaded packages to also get their dependencies
cd ${PKG_PATH}
PKG_LIST=$(ls -1 *.rpm|sed "s#-[0-9].*##")
echo "[>] redownload existing packages using yum install"
yum install   --nogpgcheck --downloadonly --downloaddir=${PKG_PATH}/ -y ${PKG_LIST}
yum reinstall --nogpgcheck --downloadonly --downloaddir=${PKG_PATH}/ -y ${PKG_LIST} || true


# # -- download remote packages via curl
# pushd ${PKG_PATH}
# for pkg in $REMOTE_PACKAGES
# do
#   curl -kLO "$pkg"
# done
# popd

# -- create repository metadata
ls -lah ${PKG_PATH}/ >${PKG_PATH}/packages.list
createrepo ${PKG_PATH}
echo "[i] packages download completed (PKG_PATH=${PKG_PATH})"

# eof
